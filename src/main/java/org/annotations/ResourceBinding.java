package org.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
public @interface ResourceBinding {
	public String bindingName() default "";

}
