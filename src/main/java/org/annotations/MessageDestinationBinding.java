package org.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
public @interface MessageDestinationBinding {
	public String bindingName() default "";
}
