package org.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
public @interface TransactionTimeoutBinding {
	public int value();
}
